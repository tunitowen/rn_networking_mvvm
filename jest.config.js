module.exports = {
    preset: "ts-jest",
    setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
    testPathIgnorePatterns: ['<rootDir>/node_modules/'],
    testEnvironment: 'jsdom',
    moduleNameMapper: {
        '\\.(css|scss)$': 'identity-obj-proxy'
    },
    globals: {
        "ts-jest": {
            tsconfig: "tsconfig.jest.json"
        }
    }
};
