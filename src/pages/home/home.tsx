import React from 'react';
import './home.css';
import './home.vm'
import useHomeViewModel from "./home.vm";
import Card from '@mui/joy/Card';
import {Button, CircularProgress, Typography} from "@mui/joy";

function Home() {

    const {getEmployees, employees, error, loading} = useHomeViewModel()

    return (
        <div className="Home">

            {(!loading) && <Button onClick={() => getEmployees()}>
                Request Employees
            </Button>}

            {(loading) && <CircularProgress/>}

            {(error !== null) && <p>Oops, an error has occurred</p>}

            {employees.map((employee, index) => {
                return <Card key={index} className={"Card"}>
                    <Typography level="title-lg">{`Name: ${employee.employee_name}`}</Typography>
                    <Typography level="body-md">{`Age:${employee.employee_age}`}</Typography>
                </Card>
            })}

        </div>
    );
}

export default Home;
