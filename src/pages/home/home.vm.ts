import {useState} from "react";
import Employee from "../../data/model/employee";
import NetworkRepository from "../../data/repos/networkRepository";

export default function useHomeViewModel(repository: NetworkRepository = NetworkRepository.getInstance()) {

    const [employees, setEmployees] = useState<Employee[]>([])
    const [error, setError] = useState<string | null>(null);
    const [loading, setLoading] = useState<boolean>(false);

    const getEmployees = () => {
        setLoading(true);
        repository.getEmployees().then((response) => {
            setLoading(false);
            setEmployees(response.data?.data ?? [])
            setError(response.error ?? null);
        });
    }

    return {
        getEmployees, employees, error, loading
    }
}
