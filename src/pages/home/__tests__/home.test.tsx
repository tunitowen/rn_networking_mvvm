import {fireEvent, render, screen} from "@testing-library/react";
import Home from "../home";

const mockGetEmployees = jest.fn()

jest.mock('../home.vm', () => {
    const originalModule = jest.requireActual('../home.vm')
    return {
        __esModule: true,
        ...originalModule,
        default: () => ({
            getEmployees: mockGetEmployees,
            employees: [{
                employee_name: "Tony",
                employee_age: 42,
                id: 2
            }],
            error: null,
            loading: false
        })
    }
})

test("Clicking the button calls getEmployees", async () => {
    render(<Home/>);
    const button = screen.getByText("Request Employees");
    fireEvent.click(button);
    expect(mockGetEmployees).toBeCalled()
});

test("Employee is displayed", async () => {
    render(<Home/>);
    const employeeItem = screen.getByText("Name: Tony");
    expect(employeeItem).toBeDefined()
});
