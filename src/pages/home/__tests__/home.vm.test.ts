import useHomeViewModel from "../home.vm";
import NetworkRepository from "../../../data/repos/networkRepository";
import {Mock, Times} from "moq.ts";
import {renderHook} from "@testing-library/react";
import {act} from "react-dom/test-utils";

test("employees should start as an empty array", () => {
    const mock = new Mock<NetworkRepository>();

    const {result} = renderHook(() => useHomeViewModel(mock.object()));

    expect(result.current.employees.length).toBe(0);
});

test("loading should start false", () => {
    const mock = new Mock<NetworkRepository>();

    const {result} = renderHook(() => useHomeViewModel(mock.object()));

    expect(result.current.loading).toBe(false);
});

test("error should start null", () => {
    const mock = new Mock<NetworkRepository>();

    const {result} = renderHook(() => useHomeViewModel(mock.object()));

    expect(result.current.error).toBeNull();
});

test("getEmployees should call the repository, and sets employees", async () => {
    const mock = new Mock<NetworkRepository>()
        .setup(instance => instance.getEmployees())
        .returnsAsync({
            data: {
                data: [
                    {employee_name: "Tony", employee_age: 44, id: 3}
                ]
            }
        });

    const {result} = renderHook(() => useHomeViewModel(mock.object()));

    await act(async () => {
        await result.current.getEmployees()
    });

    mock.verify(instance => instance.getEmployees(), Times.Once());
    expect(result.current.employees.length).toBe(1);
    expect(result.current.employees[0].employee_name).toBe("Tony");
});

test("getEmployees error should populate error", async () => {

    const mock = new Mock<NetworkRepository>()
        .setup(instance => instance.getEmployees())
        .returnsAsync({
            error: "Error 500"
        });

    const {result} = renderHook(() => useHomeViewModel(mock.object()));

    await act(async () => {
        await result.current.getEmployees()
    });

    expect(result.current.error).toBe("Error 500");
});
