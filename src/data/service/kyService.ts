import NetworkService from "./interface/networkService";
import NetworkResponse from "../utils/networkResponse";
import EmployeesResponse from "../model/employeesResponse";
import ky from 'ky';

class KyService implements NetworkService {
    async getEmployees(): Promise<NetworkResponse<EmployeesResponse>> {
        console.log("Making request with Ky");
        try {
            const response = await ky.get("https://dummy.restapiexample.com/api/v1/employees", {}).json<EmployeesResponse>()
            return Promise.resolve({data: response})
        } catch (err) {
            return Promise.resolve({error: "An error has occurred"});
        }
    }
}

export default KyService;
