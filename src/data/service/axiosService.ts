import NetworkService from "./interface/networkService";
import NetworkResponse from "../utils/networkResponse";
import EmployeesResponse from "../model/employeesResponse";
import axios from "axios";

class AxiosService implements NetworkService {
    async getEmployees(): Promise<NetworkResponse<EmployeesResponse>> {
        console.log("Making request with Axios");
        try {
            const response = await axios.get<EmployeesResponse>("https://dummy.restapiexample.com/api/v1/employees");
            return Promise.resolve({data: response.data});
        } catch (err) {
            return Promise.resolve({error: "An error occurred"});
        }

    }
}

export default AxiosService;
