import NetworkResponse from "../../utils/networkResponse";
import EmployeesResponse from "../../model/employeesResponse";

interface NetworkService {
    getEmployees: () => Promise<NetworkResponse<EmployeesResponse>>;
}

export default NetworkService;
