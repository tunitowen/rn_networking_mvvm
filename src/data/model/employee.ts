type Employee = {
    id: number;
    employee_name: string;
    employee_age: number;
}

export default Employee;
