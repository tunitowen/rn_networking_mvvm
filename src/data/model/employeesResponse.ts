import Employee from "./employee";

type EmployeesResponse = {
    data: [Employee]
}

export default EmployeesResponse;
