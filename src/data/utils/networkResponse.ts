type NetworkResponse<T> = {
    data?: T;
    error?: string;
}

export default NetworkResponse;
