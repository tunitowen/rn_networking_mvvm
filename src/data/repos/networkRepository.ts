import EmployeesResponse from "../model/employeesResponse";
import NetworkResponse from "../utils/networkResponse";
import NetworkService from "../service/interface/networkService";
import AxiosService from "../service/axiosService";
import KyService from "../service/kyService";

class NetworkRepository {

    private static instance: NetworkRepository;
    private static service: NetworkService;

    public static getInstance(networkService: NetworkService = new KyService()): NetworkRepository {
        if (!this.instance) {
            this.instance = new NetworkRepository();
            this.service = networkService;
        }
        return this.instance;
    }

    async getEmployees(): Promise<NetworkResponse<EmployeesResponse>> {
        return NetworkRepository.service.getEmployees()
    };
}

export default NetworkRepository;
