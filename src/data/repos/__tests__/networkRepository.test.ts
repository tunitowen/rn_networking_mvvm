import NetworkRepository from "../networkRepository";
import {Mock, Times} from "moq.ts";
import NetworkService from "../../service/interface/networkService";

test("getEmployees calls service and returns", async () => {

    const mock = new Mock<NetworkService>()
        .setup(instance => instance.getEmployees())
        .returnsAsync({
            data: {
                data: [
                    {employee_name: "Tony", employee_age: 22, id: 2}
                ]
            },
        });

    const repo = NetworkRepository.getInstance(mock.object());
    const result = await repo.getEmployees();

    mock.verify(instance => instance.getEmployees, Times.Once());
    expect(result.data?.data.length).toBe(1);
    expect(result.data?.data[0].employee_name).toBe("Tony")
});
